@extends('layouts.app')
@section('content')
<h1>Your Customer List:</h1>
<a href = "{{route('customers.create')}}">Add a new Customer </a>
<ul>
@foreach($customers as $customer)
@if($customer->status==0)
@else 
    <li style ="color:green" font-weight: bold>
@endif
{{$customer->name}}, {{$customer->email}}, {{$customer->phone}}, Created by: {{$customer->user->name}}
    <a href = "{{route('customers.edit',$customer->id)}}">Edit</a>
    @can('manager')
        <a method="post" href="{{route('delete',$customer->id)}}">delete</a>
        @endcan
        @cannot('manager')
        delete
        @endcannot('manager')

        @can('manager')
                    @if ($customer->status== 0)
                        <a href="{{route('update_status',$customer->id)}}">deal closed</a>
                    @endif
                @endcan
    </li>

</li>
@endforeach
</ul>
@endsection