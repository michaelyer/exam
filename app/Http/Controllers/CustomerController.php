<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Customer;


class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $id = Auth::id();
         $user = User::Find($id);
         if($user==null){
             return view('welcome');
         }
        $customers = Customer::All();
        $user_name = DB::table('users')->whereExists(function ($query) {
            $query->select('name')->from('customers')
                ->whereRaw('customers.user_id = users.id');
    })->get();
    $current_user = $user->id;
    return view('customers.index',compact('customers','user_name','current_user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id = Auth::id();
        $user = User::find($id); 
        // if($user == null){
            // return view('welcome');
        return view('customers.create');
    }
// }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customers = new Customer();
        $id = Auth::id(); 
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->phone = $request->phone;
        $customers->user_id = $id;
        $customers->save();
        return redirect('customers');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id = Auth::id();
        $customers = Customer::find($id); 
        if(Gate::denies('manager')){
            if ($user_id != $customers->user_id){
            abort(403,"Sorry you do not hold premission to edit this customer");
            return view('welcome');
        }
    }

        $customers=Customer::find($id);
        return view('customers.edit',['customers'=>$customers]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customers = Customer::findOrFail($id);
        $customers->update($request->except(['_token']));
        return redirect('customers'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')){
            abort(403,"Sorry you do not have the premission to delete customers");
        }
        $customers=Customer::find($id);
        $customers->delete();
        return redirect('customers');
    }


    public function update_status($id){
        if(Gate::denies('manager')){
            abort(403,"Sorry you do not have the premission to change customer's status");
        }
        $customers=Customer::find($id);
        $customers->status=1;
        $customers->save();
        return redirect('customers');
    }
}
