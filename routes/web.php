<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::resource('customers','CustomerController')->middleware('auth');
Auth::routes();

Route::get('customers/delete/{id}', 'CustomerController@destroy')->name('delete');

Route::get('customer/update_status/{id}','CustomerController@update_status')->name('update_status');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('registerfail',function(){

    return view('customers.users_only');
})->name('users_only');
