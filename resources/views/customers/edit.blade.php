@extends('layouts.app')
@section('content')
<h1>Edit Existing Customer</h1>
<form method = 'POST' action = "{{action('CustomerController@update' , $customers->id)}}">
@method('PATCH')
@csrf
<div class = "form-group">
<label for = "title">Customer Name:</label>
<input type = "text" class= "form-control" name = "name" value = "{{$customers->name}}">
</div>
<div class = "form-group">
<label for = "title">Customer Email:</label>
<input type = "text" class= "form-control" name = "email" value = "{{$customers->email}}">
</div>
<div class = "form-group">
<label for = "title">Customer Phone Number:</label>
<input type = "text" class= "form-control" name = "phone" value = "{{$customers->phone}}">
</div>
<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit" value = "Save">
</div>
</form>
@endsection