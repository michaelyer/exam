@extends('layouts.app')
@section('content')
<h1>Create a new Customer</h1>
<form method = 'POST' action = "{{action('CustomerController@store')}}">
@csrf
<div class = "form-group">
<label for = "title">Customer's Name:</label>
<input type = "text" class= "form-control" name = "name">
</div>
<div class = "form-group">
<label for = "title">Customer's Email:</label>
<input type = "text" class= "form-control" name = "email">
</div>
<div class = "form-group">
<label for = "title">Customer's Phone Number:</label>
<input type = "text" class= "form-control" name = "phone">
</div>
<div class = "form-group">
<input type = "submit" class= "form-control" name = "submit">
</div>
</form>
@endsection